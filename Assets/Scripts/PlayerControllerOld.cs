﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerOld : MonoBehaviour
{
    public float moveSpeed = 8f;
    public float turnSpeed = 100f;
    public float jumpForce = 10f;
    public float rotationSmoothTime = 0.1f;
    
    private enum ControlAction {MoveUp, MoveDown, MoveLeft, MoveRight, Jump}
    private Dictionary<ControlAction, List<KeyCode>> controlKeys;
    private float rotationDeg = 0f;
    private new Rigidbody rigidbody;
    private float currentRotationVelocity = 0f;

    // fields for transfering input state between Update and FixedUpdate
    private Vector3 movementInput = Vector3.zero;
    private bool jump = false;

    public PlayerControllerOld()
    {
        controlKeys = new Dictionary<ControlAction, List<KeyCode>>();
        controlKeys.Add(ControlAction.MoveUp, new List<KeyCode> {KeyCode.W, KeyCode.UpArrow});
        controlKeys.Add(ControlAction.MoveDown, new List<KeyCode> {KeyCode.S, KeyCode.DownArrow});
        controlKeys.Add(ControlAction.MoveLeft, new List<KeyCode> {KeyCode.A, KeyCode.LeftArrow});
        controlKeys.Add(ControlAction.MoveRight, new List<KeyCode> {KeyCode.D, KeyCode.RightArrow});
        controlKeys.Add(ControlAction.Jump, new List<KeyCode> {KeyCode.Space});
    }


    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        movementInput = GetMovementInput();
        RegisterJumpInput();
    }

    
    // Update is called once per frame
    void FixedUpdate()
    {
        Move();
        Jump();
    }

    private void Move()
    {
        if (movementInput != Vector3.zero)
        {
            var verticalVelocity = rigidbody.velocity.y;
            var newVelocity = movementInput * moveSpeed;
            newVelocity.y = rigidbody.velocity.y;
            rigidbody.velocity = newVelocity;

            rotationDeg = GetPlayerRotationByMoveDirection(movementInput);
            RotatePlayerModel(rotationDeg);
        }
    }

    private void Jump()
    {
        if (jump)
        {
            var distanceToFloorStanding = transform.lossyScale.y / 2 + (float)0.1;
            var hit = Physics.Raycast(transform.position, Vector3.down, out RaycastHit hitInfo, distanceToFloorStanding);
            if (hit && hitInfo.collider?.gameObject.name == "Floor")
            {
                rigidbody.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
                Debug.Log("jump");
            }
            jump = false;
        }
    }

    private Vector3 GetMovementInput() {
        var input = Vector3.zero;
        foreach (var key in controlKeys[ControlAction.MoveUp])
        {
            if (Input.GetKey(key)) {
                input.z = 1;
                break;
            }
        }
        foreach (var key in controlKeys[ControlAction.MoveDown])
        {
            if (Input.GetKey(key)) {
                input.z = -1;
                break;
            }
        }
        foreach (var key in controlKeys[ControlAction.MoveLeft])
        {
            if (Input.GetKey(key)) {
                input.x = -1;
                break;
            }
        }
        foreach (var key in controlKeys[ControlAction.MoveRight])
        {
            if (Input.GetKey(key)) {
                input.x = 1;
                break;
            }
        }

        return input;
    }

    private void RegisterJumpInput()
    {
        foreach (var key in controlKeys[ControlAction.Jump])
        {
            if (Input.GetKeyDown(key))
            {
                jump = true;
                break;
            }
        }
    }


    private float GetPlayerRotationByMoveDirection(Vector3 directions) {
        float angle = Mathf.Atan2(directions.x, directions.z) * Mathf.Rad2Deg;
        return angle;
    }
    

    private void RotatePlayerModel(float degrees) {
        var currentRotation = transform.localRotation.eulerAngles.y;
        var rotationAngle = Mathf.SmoothDampAngle(currentRotation, degrees, ref currentRotationVelocity, rotationSmoothTime);
        transform.localRotation = Quaternion.Euler(0, rotationAngle, 0);
    }
}
