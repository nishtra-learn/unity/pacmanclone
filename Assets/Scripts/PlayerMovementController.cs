﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class PlayerMovementController
{
    protected PlayerMovementData movementParams;
    protected GameObject player;
    protected Rigidbody rigidbody;

    // fields for transfering input state between Update and FixedUpdate
    protected Vector2 movementInput = Vector2.zero;
    protected Vector2 rotationInput = Vector2.zero;
    protected bool jump = false;


    public PlayerMovementController(GameObject player, PlayerMovementData movementParameters) {
        this.player = player;
        this.rigidbody = player.GetComponents<Rigidbody>().FirstOrDefault();
        this.movementParams = movementParameters;
    }


    protected abstract void Move();
    
    public virtual void OnUpdate()
    {
        movementInput = GetMovementInput();
        rotationInput = GetRotationInput();
        RegisterJumpInput();
    }

    
    // Update is called once per frame
    public virtual void OnFixedUpdate()
    {
        Move();
        Jump();
    }

    private Vector2 GetMovementInput() {
        var input = Vector2.zero;
        input.x = Input.GetAxisRaw("Horizontal");
        input.y = Input.GetAxisRaw("Vertical");
        
        return input;
    }

    private Vector2 GetRotationInput() {
        var input = Vector2.zero;
        input.x = Input.GetAxisRaw("Mouse X") * movementParams.turnSpeed * Time.deltaTime;
        input.y = Input.GetAxisRaw("Mouse Y") * movementParams.turnSpeed * Time.deltaTime;

        return input;
    }

    private void RegisterJumpInput()
    {
        if (Input.GetAxisRaw("Jump") > 0)
            jump = true;
    }

    private void Jump()
    {
        if (jump)
        {
            var distanceToFloorStanding = player.transform.lossyScale.y / 2 + (float)0.1;
            var hit = Physics.Raycast(player.transform.position, Vector3.down, out RaycastHit hitInfo, distanceToFloorStanding);
            if (hit && hitInfo.collider?.gameObject.name == "Floor")
            {
                rigidbody.AddForce(Vector3.up * movementParams.jumpForce, ForceMode.VelocityChange);
                Debug.Log("jump");
            }
            jump = false;
        }
    }

    
}
