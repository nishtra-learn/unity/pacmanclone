﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollTexture : MonoBehaviour
{
    private new Renderer renderer;
    private Material material;
    
    public Vector2 scrollDirection = Vector2.right;
    public float scrollSpeed = 5f;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
        material = renderer.material;
    }

    // Update is called once per frame
    void Update()
    {
        material.mainTextureOffset += scrollDirection * scrollSpeed * Time.deltaTime;
    }
}
