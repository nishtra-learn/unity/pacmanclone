﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton setup
    private static GameManager instance;

    public static GameManager Instance { get { return instance; } }
    #endregion

    private int totalCrystals;
    
    private int crystals;
    public int Crystals
    {
        get => crystals;
        set
        {
            crystals = value;
            OnCrystalGet();
        }
    }

    private int enemiesCount; // including enemy reserves in spawners
    public int EnemiesCount
    {
        get => enemiesCount; set
        {
            enemiesCount = value;
            CheckEnemyTriggers();
        }
    }



    private void Awake()
    {
        #region Singleton setup
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        #endregion

    }

    private void Start()
    {
        totalCrystals = GameObject.FindGameObjectsWithTag("Crystal").Length;
        Debug.Log($"Total crystals: {totalCrystals}");
        enemiesCount = GameObject.FindObjectsOfType<SpawnEnemy>()
            .Aggregate(0, (acc, i) => acc += i.spawnReserves);
        // subscribe to character death
        var playerCharacter = GameObject.FindObjectOfType<Character>();
        if (playerCharacter != null)
            playerCharacter.OnDeath += Lose;
    }

    private void OnCrystalGet()
    {
        Debug.Log($"Current crystals: {Crystals}");
        if (Crystals == 10) {
            // drop enemy damaging force field
            var cage = GameObject.Find("Cage");
            var rb = cage.GetComponent<Rigidbody>();
            if (rb?.isKinematic == true) {
                rb.isKinematic = false;
            }
            // change global lighting while the forcefield is falling
            StartCoroutine(ChangeGlobalLight());
        }

        if (Crystals == totalCrystals) {
            Win();
        }
    }

    private void CheckEnemyTriggers()
    {
        if (EnemiesCount <= 0)
            Win();
    }

    private void Win()
    {
        Debug.LogWarning("YOU WIN");
    }

    private void Lose() {
        Debug.Log("YOU LOSE");
    }

    private IEnumerator ChangeGlobalLight() {
        var globalLighting = GameObject.Find("Directional Light").GetComponent<Light>();
        var defaultLightColor = globalLighting.color;
        var targetColor = Color.blue;
        yield return StartCoroutine(GradualLightChange(globalLighting, targetColor, 0.3f));
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(GradualLightChange(globalLighting, defaultLightColor, 1.5f));
    }

    private IEnumerator GradualLightChange(Light light, Color targetColor, float duration) {
        float timeElapsed = 0;
        var startColor = light.color;
        while (timeElapsed < duration) {
            var newColor = Color.Lerp(startColor, targetColor, timeElapsed / duration);
            light.color = newColor;
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        light.color = targetColor;
    }

}
