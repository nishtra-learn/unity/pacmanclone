﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Transform target;
    public float aggroDistance = 10;
    public float moveSpeed = 3;
    public float turnSpeed = 300f;

    // Start is called before the first frame update
    void Start()
    {
        var teleports = FindObjectsOfType<GateTeleport>();
        foreach (var item in teleports)
        {
            Debug.Log(item.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        var distanceToTarget = Vector3.Distance(transform.position, target.position);
        var distanceToTargetThroughTeleport = distanceToTarget;
        GateTeleport useTeleport = null;
        foreach (var tp in TeleportManager.Instance.Teleports)
        {
            var distToTeleport = Vector3.Distance(transform.position, tp.GetSelfPosition());
            var distFromTeleportLocationToTarget = Vector3.Distance(tp.GetTargetPosition(), target.position);
            var dist = distToTeleport + distFromTeleportLocationToTarget;
            if (dist < distanceToTargetThroughTeleport) {
                distanceToTargetThroughTeleport = dist;
                useTeleport = tp;
            }
        }

        if (distanceToTarget < aggroDistance || distanceToTargetThroughTeleport < aggroDistance)
        {
            Vector3 moveTarget = target.position;
            if (useTeleport) {
                moveTarget = useTeleport.GetSelfPosition();
            }
            RotateTowardsTarget(moveTarget);
            MoveTowardsTarget(moveTarget);
        }
    }

    private void MoveTowardsTarget(Vector3 targetPosition)
    {
        Vector3 moveVector = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);
        transform.position = moveVector;
    }

    private void RotateTowardsTarget(Vector3 targetPosition)
    {
        Vector3 targetDirection = targetPosition - transform.position;
        targetDirection.y = 0;
        float rotationStep = turnSpeed * Time.deltaTime;
        Vector3 lookVector = Vector3.RotateTowards(transform.forward, targetDirection, rotationStep, 0.0f);
        transform.rotation = Quaternion.LookRotation(lookVector);
    }


}
