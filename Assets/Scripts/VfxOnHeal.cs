﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VfxOnHeal : MonoBehaviour
{
    [SerializeField]
    private float blinkFrequency = 2f;
    [SerializeField]
    private Renderer effectRenderer;
    [SerializeField]
    private GameObject healLightPrefab;

    private bool isHealing;
    private Color originalColor;
    private GameObject healLightObject;
    

    void Awake()
    {
        var character = GetComponent<ICharacter>();
        if (character != null) {
            character.StatusEffectAdded += (effect) => {
                if (effect.GetType() == typeof(StatusEffects.Heal)) {
                    isHealing = true;
                    effectRenderer.gameObject.SetActive(true);
                    healLightObject = Instantiate(healLightPrefab, transform.position, Quaternion.identity);
                    healLightObject.transform.parent = gameObject.transform;
                }
            };
            character.StatusEffectRemoved += (effect) => {
                if (effect.GetType() == typeof(StatusEffects.Heal)) {
                    isHealing = false;
                    effectRenderer.material.SetColor("_EmissionColor", originalColor);
                    effectRenderer.gameObject.SetActive(false);
                    healLightObject.SetActive(false);
                    Destroy(healLightObject);
                }
            };
        }

        originalColor = effectRenderer.material.color;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isHealing || effectRenderer == null)
            return;

        var emission = Mathf.PingPong(Time.time, blinkFrequency);
        effectRenderer.material.SetColor("_EmissionColor", effectRenderer.material.color * emission);
    }
}
