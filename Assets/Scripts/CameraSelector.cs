﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraSelector : MonoBehaviour
{
    private GameObject topDownCamera;
    private GameObject firstPersonCamera;
    private GameObject thirdPersonCamera;
    private GameObject activeCamera;
    private GameObject player;
    private PlayerController playerController;
    private bool searchedForCameras = false;

    
    // Update is called once per frame
    void Update()
    {
        if (!searchedForCameras) {
            FindCameras();
            searchedForCameras = true;
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (topDownCamera == null)
                throw new MissingReferenceException("Top-down view camera was not found!");
            activeCamera?.SetActive(false);
            topDownCamera.SetActive(true);
            activeCamera = topDownCamera;
            playerController.SwitchToTopDownControls();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (topDownCamera == null)
                throw new MissingReferenceException("First person view camera was not found!");
            activeCamera?.SetActive(false);
            firstPersonCamera.SetActive(true);
            activeCamera = firstPersonCamera;
            playerController.SwitchToFirstPersonControls();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (topDownCamera == null)
                throw new MissingReferenceException("Third person view camera was not found!");
            activeCamera?.SetActive(false);
            thirdPersonCamera.SetActive(true);
            activeCamera = thirdPersonCamera;
            playerController.SwitchToFirstPersonControls();
        }
    }

    private void FindCameras()
    {
        topDownCamera = GameObject.Find("TopDownCamera");
        playerController = FindObjectOfType<PlayerController>();
        player = playerController?.gameObject;
        var playerAttachedCameras = player?.GetComponentsInChildren<Camera>(true);
        firstPersonCamera = playerAttachedCameras.FirstOrDefault(c => c.gameObject.name == "FirstPersonCamera")?.gameObject;
        thirdPersonCamera = playerAttachedCameras.FirstOrDefault(c => c.gameObject.name == "ThirdPersonCamera")?.gameObject;
        
        activeCamera = topDownCamera;
    }
}
