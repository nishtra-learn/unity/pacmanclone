﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VfxManager : MonoBehaviour
{
    #region Singleton setup
    private static VfxManager instance;

    public static VfxManager Instance { get { return instance; } }
    #endregion

    [SerializeField]
    private ParticleSystem deathVfx;
    public ParticleSystem DeathVfx { get => deathVfx; }
    
    
    private void Awake()
    {
        #region Singleton setup
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        } else {
            instance = this;
        }
        #endregion
    }
}
