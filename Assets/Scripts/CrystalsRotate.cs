﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CrystalsRotate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var crystals = gameObject.GetComponentsInChildren<Transform>().Where(t => t.tag == "Crystal");
        foreach (var item in crystals)
        {
            item.Rotate(new Vector3(45, 45, 45) * Time.deltaTime);
        }
    }
}
