﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportManager : MonoBehaviour
{
    #region Singleton setup
    private static TeleportManager instance;

    public static TeleportManager Instance { get { return instance; } }
    #endregion


    public GateTeleport[] Teleports { get; private set; }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        } else {
            instance = this;
        }
    }

    private void Start() {
        Teleports = GameObject.FindObjectsOfType<GateTeleport>();
    }
}
