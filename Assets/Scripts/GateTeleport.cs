﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateTeleport : MonoBehaviour
{
    public Transform target = null;

    private void Start() {
        var emitterAreaScale = transform.localScale.x * transform.localScale.y;
        var particleSystem = GetComponentInChildren<ParticleSystem>();
        var emission = particleSystem.emission;
        emission.rateOverTimeMultiplier *= emitterAreaScale;
    }

    private void OnTriggerEnter(Collider other) {
        if (target == null) {
            return;
        }

        if (other.gameObject.tag != "Enemy" && other.gameObject.name != "Player") {
            return;
        }
        
        
        var actor = other.transform;
        var newPosition = target.position;
        newPosition.y = actor.position.y;
        
        // move by Transform
        //actor.position = newPosition;

        // move rigidbody
        var actorRigidbody = other.gameObject.GetComponent<Rigidbody>();
        actorRigidbody.isKinematic = true;
        actor.position = newPosition;
        actorRigidbody.isKinematic = false;
    }

    public Vector3 GetTargetPosition() {
        var pos = target.position;
        return pos;
    }

    public Vector3 GetSelfPosition() {
        var pos = transform.position;
        return pos;
    }
}
