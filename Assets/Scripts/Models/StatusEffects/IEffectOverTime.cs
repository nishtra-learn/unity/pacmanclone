﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StatusEffects
{
    public interface IEffectOverTime
    {
        int EffectEveryNTick { get; set; }
        int Potency { get; set; }
    }
}