﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StatusEffects
{
    public class Heal : StatusEffect, IEffectOverTime
    {
        public int EffectEveryNTick { get; set; }
        public int Potency { get; set; }
        private int tick;

        public Heal(int durationSeconds, int potency, int effectEveryNTick)
        {
            this.TimeLeftSeconds = durationSeconds;
            this.Potency = potency;
            this.EffectEveryNTick = effectEveryNTick;
        }

        public override void Tick(ICharacter target, bool logEffect = false)
        {
            base.Tick(target);
            tick++;
            if (tick == EffectEveryNTick)
            {
                if (logEffect)
                {
                    Debug.Log($"[{Time.time}] Heal character for {Potency} health");
                }
                
                target.HP = Mathf.Clamp(target.HP + Potency, 0, target.MaxHP);
                tick = 0;
            }
        }
    }
}