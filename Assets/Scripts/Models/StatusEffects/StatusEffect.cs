﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StatusEffects
{
    public abstract class StatusEffect
    {
        public static readonly float timerTickInterval = 0.25f;

        public float TimeLeftSeconds { get; set; }
        public bool LogEffect {get; set;}

        public virtual void Tick(ICharacter target, bool logEffect = false)
        {
            TimeLeftSeconds -= timerTickInterval;
        }
    }
}