﻿using System.Collections;
using System.Collections.Generic;
using StatusEffects;
using UnityEngine;

public class Enemy : MonoBehaviour, ICharacter
{
    private List<StatusEffect> statusEffects = new List<StatusEffect>();

    [SerializeField]
    private int maxHp = 100;
    public int MaxHP { get => maxHp; set => maxHp = value; }

    [SerializeField]
    private int hp;
    [SerializeField]
    private GameObject deathLightEffectPrefab;

    private GameObject deathLightEffectObject;
    public event System.Action<StatusEffect> StatusEffectAdded;
    public event System.Action<StatusEffect> StatusEffectRemoved;

    public int HP
    {
        get => hp; 
        set
        {
            hp = Mathf.Clamp(value, 0, MaxHP);
            OnHpChange();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        HP = MaxHP;
        StartCoroutine(StatusEffectTimer());
    }

    public StatusEffect[] GetActiveStatusEffects() {
        return statusEffects.ToArray();
    }

    public void AddStatusEffect(StatusEffect effect)
    {
        var hasEffectOfThisType = statusEffects.Exists(ef => ef.GetType() == effect.GetType());
        if (!hasEffectOfThisType)
        {
            statusEffects.Add(effect);
            StatusEffectAdded?.Invoke(effect);

            // remove health regen if setting Burn
            if (effect.GetType() == typeof(Burn))
            {
                var healthRegen = statusEffects.Find(ef => ef.GetType() == typeof(Heal));
                if (healthRegen != null)
                {
                    RemoveStatusEffect(healthRegen);
                }
            }
        }
    }

    public void RemoveStatusEffect(StatusEffect effect)
    {
        statusEffects.Remove(effect);
        StatusEffectRemoved?.Invoke(effect);
    }

    private IEnumerator StatusEffectTimer()
    {
        while (true)
        {
            // enemy specific
            // activate health regen if not burning
            var isBurningOrHealing = statusEffects.Exists(ef => ef.GetType() == typeof(Burn) || ef.GetType() == typeof(Heal));
            if (!isBurningOrHealing)
            {
                var healthRegen = new StatusEffects.Heal(999, 2, 4);
                AddStatusEffect(healthRegen);
            }

            // tick all effects
            foreach (var effect in statusEffects)
            {
                effect.Tick(this);
            }
            // remove expired
            for (int i = 0; i < statusEffects.Count; i++)
            {
                var ef = statusEffects[i];
                if (ef.TimeLeftSeconds <= 0)
                        RemoveStatusEffect(ef);
            }
            yield return new WaitForSeconds(StatusEffect.timerTickInterval);
        }
    }

    private void OnHpChange()
    {
        if (HP <= 0) {
            Die();
        }
    }

    private void Die()
    {
        var particles = Instantiate<ParticleSystem>(VfxManager.Instance.DeathVfx, transform.position, Quaternion.identity);
        StartCoroutine(LightFlashOnDeath());
    }

    private IEnumerator LightFlashOnDeath() {
        var startIntensity = 1.5f;
        var endIntensity = 0f;
        var startRange = 0.5f;
        var endRange = 5f;
        var duration = 0.2f;

        deathLightEffectObject = Instantiate(deathLightEffectPrefab, transform.position, Quaternion.identity);
        deathLightEffectObject.transform.parent = gameObject.transform;
        var deathLight = deathLightEffectObject.GetComponent<Light>();
        deathLight.intensity = startIntensity;
        deathLight.range = startRange;

        var timeElapsed = 0f;
        while (timeElapsed < duration)
        {
            var intensity = Mathf.Lerp(startIntensity, endIntensity, timeElapsed / duration);
            var range = Mathf.Lerp(startRange, endRange, timeElapsed / duration);
            deathLight.intensity = intensity;
            deathLight.range = range;
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        deathLightEffectObject.SetActive(false);
        Destroy(deathLightEffectObject);
        gameObject.SetActive(false);
        Destroy(gameObject);
    }
}
