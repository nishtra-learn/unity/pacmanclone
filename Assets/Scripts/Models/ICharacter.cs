﻿using System;
using System.Collections;
using System.Collections.Generic;
using StatusEffects;

public interface ICharacter
{
    int MaxHP { get; set; }
    int HP { get; set; }
    event Action<StatusEffect> StatusEffectAdded;
    event Action<StatusEffect> StatusEffectRemoved;

    StatusEffect[] GetActiveStatusEffects();
    void AddStatusEffect(StatusEffect effect);
    void RemoveStatusEffect(StatusEffect effect);
}
