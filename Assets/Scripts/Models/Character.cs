﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StatusEffects;
using System;
using System.Linq;

public class Character : MonoBehaviour, ICharacter
{
    private List<StatusEffect> statusEffects = new List<StatusEffect>();

    [SerializeField]
    private int maxHp = 100;
    public int MaxHP { get => maxHp; set => maxHp = value; }

    [SerializeField]
    private int hp;
    public int HP
    {
        get => hp; 
        set
        {
            hp = Mathf.Clamp(value, 0, MaxHP);
            OnHpChange();
        }
    }

    public event Action OnDeath;
    public event Action<StatusEffect> StatusEffectAdded;
    public event Action<StatusEffect> StatusEffectRemoved;

    // Start is called before the first frame update
    void Start()
    {
        HP = MaxHP;
        StartCoroutine(StatusEffectTimer());
    }

    public StatusEffect[] GetActiveStatusEffects() {
        return statusEffects.ToArray();
    }

    public void AddStatusEffect(StatusEffect effect)
    {
        var hasEffectOfThisType = statusEffects.Exists(ef => ef.GetType() == effect.GetType());
        if (hasEffectOfThisType)
            return;
        
        statusEffects.Add(effect);
        StatusEffectAdded?.Invoke(effect);
    }

    public void RemoveStatusEffect(StatusEffect effect)
    {
        statusEffects.Remove(effect);
        StatusEffectRemoved?.Invoke(effect);
    }

    private IEnumerator StatusEffectTimer()
    {
        while (true)
        {
            // tick all effects
            foreach (var effect in statusEffects)
            {
                effect.Tick(this);
            }
            // remove expired
            for (int i = 0; i < statusEffects.Count; i++)
            {
                var ef = statusEffects[i];
                if (ef.TimeLeftSeconds <= 0)
                        RemoveStatusEffect(ef);
            }
            yield return new WaitForSeconds(StatusEffect.timerTickInterval);
        }
    }

    private void OnHpChange()
    {
        if (HP <= 0) {
            Die();
        }
    }

    private void Die()
    {
        Debug.Log("\"Oh I'm die. Thank you forever\"");
        OnDeath?.Invoke();
        
        var vfx = Instantiate<ParticleSystem>(VfxManager.Instance.DeathVfx, transform.position, Quaternion.identity);
    }

    private void Update() {
        
    }
}
