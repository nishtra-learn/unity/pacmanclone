﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VfxOnTakingDamage : MonoBehaviour
{
    [SerializeField]
    private float blinkFrequency = 0.5f;
    [SerializeField]
    private float maxBlinkIntensity = 2f;
    [SerializeField]
    private GameObject burnLightPrefab;
    [SerializeField]
    private float burnLightMinIntensity = 0.9f;
    [SerializeField]
    private float burnLightMaxIntensity = 2.1f;
    [SerializeField]
    private float burnLightMinRange = 1.6f;
    [SerializeField]
    private float burnLightMaxRange = 1.9f;
    
    protected Renderer effectRenderer;
    private bool isBurning;
    private GameObject burnLightObject;
    private Light burnLight;
    private Color originalColor;

    protected virtual void SetBlinkRenderer() {
        effectRenderer = gameObject.GetComponent<Renderer>();
    }
    

    void Awake()
    {
        SetBlinkRenderer();
        
        var character = GetComponent<ICharacter>();
        if (character != null) {
            character.StatusEffectAdded += (effect) => {
                if (effect.GetType() == typeof(StatusEffects.Burn)) {
                    isBurning = true;
                    effectRenderer.material.SetColor("_EmissionColor", new Color(0.4f, 0f, 0f));
                    burnLightObject = Instantiate(burnLightPrefab, transform.position, Quaternion.identity);
                    burnLightObject.transform.parent = gameObject.transform;
                    burnLight = burnLightObject.GetComponent<Light>();
                }
            };
            character.StatusEffectRemoved += (effect) => {
                if (effect.GetType() == typeof(StatusEffects.Burn)) {
                    isBurning = false;
                    effectRenderer.material.SetColor("_EmissionColor", originalColor);
                    burnLightObject.SetActive(false);
                    Destroy(burnLightObject);
                }
            };
        }

        originalColor = effectRenderer.material.GetColor("_EmissionColor");
    }

    // Update is called once per frame
    void Update()
    {
        if (!isBurning)
            return;

        var percent = Mathf.PingPong(Time.time, blinkFrequency) / blinkFrequency;
        if (effectRenderer != null) {
            var minIntensityColor = originalColor;
            var maxIntensityColor = originalColor * maxBlinkIntensity;
            var color = Color.Lerp(minIntensityColor, maxIntensityColor, percent);
            effectRenderer.material.SetColor("_EmissionColor", color);
        }

        if (burnLight != null) {
            var intensity = Mathf.Lerp(burnLightMinIntensity, burnLightMaxIntensity, percent);
            burnLight.intensity = intensity;

            var range = Mathf.Lerp(burnLightMinRange, burnLightMaxRange, percent);
            burnLight.range = range;
        }
        
    }
}
