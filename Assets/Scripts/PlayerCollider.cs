﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Crystal") {
            ProcessCrystalCollision(other);
        }
    }

    private void ProcessCrystalCollision(Collider other) {
        other.enabled = false;
        Destroy(other.gameObject);
        GameManager.Instance.Crystals++;
    }

}
