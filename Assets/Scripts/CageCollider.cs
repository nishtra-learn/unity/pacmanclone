﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CageCollider : MonoBehaviour
{
    private LinkedList<Collider> recentCollisions = new LinkedList<Collider>();

    private void OnTriggerStay(Collider other) {
        if (other.tag == "Enemy")
            ProcessEnemy(other);
    }

    private void ProcessEnemy(Collider other)
    {
        if (recentCollisions.Contains(other))
            return;
    
        StartCoroutine(DealDamageToEnemy(other));
    }

    private IEnumerator DealDamageToEnemy(Collider enemyCollider) {
        var enemy = enemyCollider.gameObject.GetComponent<Enemy>();
        enemy.HP = Mathf.Clamp(enemy.HP - 10, 0, 100); 
        recentCollisions.AddLast(enemyCollider);
        yield return new WaitForSeconds(1f);
        recentCollisions.Remove(enemyCollider);
    }
}
