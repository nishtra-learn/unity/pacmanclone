﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementTopDownController : PlayerMovementController
{
    private float currentRotationVelocity = 0f;

    public PlayerMovementTopDownController(GameObject player, PlayerMovementData movementParameters) 
        : base(player, movementParameters)
    {  }

    protected override void Move()
    {
        if (movementInput != Vector2.zero)
        {
            var newVelocity = new Vector3(movementInput.x, 0, movementInput.y) * movementParams.moveSpeed;
            newVelocity.y = rigidbody.velocity.y;
            rigidbody.velocity = newVelocity;

            var rotationDeg = GetPlayerRotationByMoveDirection(movementInput);
            RotatePlayerModel(rotationDeg);
        }
    }

    private float GetPlayerRotationByMoveDirection(Vector2 directions) {
        float angle = Mathf.Atan2(directions.x, directions.y) * Mathf.Rad2Deg;
        return angle;
    }
    

    private void RotatePlayerModel(float degrees) {
        var currentRotation = player.transform.localRotation.eulerAngles.y;
        var rotationAngle = Mathf.SmoothDampAngle(currentRotation, degrees, ref currentRotationVelocity, movementParams.rotationSmoothTime);
        player.transform.localRotation = Quaternion.Euler(0, rotationAngle, 0);
    }
}
