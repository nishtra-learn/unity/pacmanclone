﻿using UnityEngine;

[CreateAssetMenu(menuName = "Player Movement Data", fileName = "PlayerMovementData")]
public class PlayerMovementData : ScriptableObject
{
    public float moveSpeed = 8f;
    public float strafingSpeed = 4f;
    public float turnSpeed = 500f;
    public float jumpForce = 10f;
    public float rotationSmoothTime = 0.1f;
}
