﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCollider : MonoBehaviour
{
    private LinkedList<Collider> recentCollisions = new LinkedList<Collider>();

    private void OnTriggerStay(Collider other)
    {
        var character = other.gameObject.GetComponent<ICharacter>();
        if (character == null)
            return;

        if (!recentCollisions.Contains(other))
        {
            recentCollisions.AddLast(other);
            Debug.Log("Fire trigger check");
            StartCoroutine(DelayCollisionCheck());
            
            int twoHitsPerSecond = (int)(1 / StatusEffects.StatusEffect.timerTickInterval / 2);
            var burnEffect = new StatusEffects.Burn(3, 5, twoHitsPerSecond);
            character.AddStatusEffect(burnEffect);
        }
    }

    private IEnumerator DelayCollisionCheck()
    {

        yield return new WaitForSeconds(0.25f);
        recentCollisions.Clear();
    }
}
