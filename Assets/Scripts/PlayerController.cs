﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private PlayerMovementData movementParams;
    private PlayerMovementTopDownController topDownController;
    private PlayerMovementFirstPersonController firstPersonController;
    private PlayerMovementController activeMovementController;


    // Start is called before the first frame update
    private void Start()
    {
        topDownController = new PlayerMovementTopDownController(gameObject, movementParams);
        firstPersonController = new PlayerMovementFirstPersonController(gameObject, movementParams);
        activeMovementController = topDownController;
    }

    private void Update()
    {
        activeMovementController.OnUpdate();
    }

    private void FixedUpdate()
    {
        activeMovementController.OnFixedUpdate();
    }

    public void SwitchToFirstPersonControls()
    {
        activeMovementController = firstPersonController;
        Cursor.visible = false;
    }

    public void SwitchToTopDownControls()
    {
        activeMovementController = topDownController;
        Cursor.visible = true;
    }
}
