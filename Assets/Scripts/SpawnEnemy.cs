﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    public int aggroDistance;
    public Transform player;
    public GameObject enemyPrefab;
    public int spawnReserves = 1;
    
    //private GameObject activeEnemy = null;
    private bool isOnCooldown = false;


    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var distanceToPlayer = Vector3.Distance(transform.position, player.position);
        if (distanceToPlayer < aggroDistance && spawnReserves > 0 && !isOnCooldown)
        {
            var enemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
            enemy.GetComponent<EnemyController>().target = player;
            spawnReserves--;
            if (spawnReserves == 0) {
                Deactivate();
            }
            StartCoroutine(SpawnCooldown());
        }
    }

    private void Deactivate()
    {
        var renderer = GetComponentInChildren<Renderer>();
        renderer.material.SetColor("_EmissionColor", Color.gray);
    }

    private IEnumerator SpawnCooldown() {
        isOnCooldown = true;
        yield return new WaitForSeconds(3f);
        isOnCooldown = false;
    }
}
