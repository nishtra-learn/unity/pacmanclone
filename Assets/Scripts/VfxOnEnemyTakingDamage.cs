﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VfxOnEnemyTakingDamage : VfxOnTakingDamage
{
    protected override void SetBlinkRenderer() {
        var blinkRenderer = GetComponentInChildren<Renderer>();
        effectRenderer = blinkRenderer;
    }
}
