﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealFieldCollider : MonoBehaviour
{
    private bool checkForCollision = true;

    private void OnTriggerStay(Collider other)
    {
        if (checkForCollision)
        {
            if (other.name != "Player")
                return;

            checkForCollision = false;
            Debug.Log("Heal trigger check");
            StartCoroutine(DelayCollisionCheck());

            var character = other.gameObject.GetComponent<Character>();
            if (character)
            {
                int oneHitPerSecond = (int)(1 / StatusEffects.StatusEffect.timerTickInterval);
                var regenerationEffect = new StatusEffects.Heal(2, 5, oneHitPerSecond);
                character.AddStatusEffect(regenerationEffect);
            }
        }
    }

    private IEnumerator DelayCollisionCheck()
    {

        yield return new WaitForSeconds(0.25f);
        checkForCollision = true;
    }
}
