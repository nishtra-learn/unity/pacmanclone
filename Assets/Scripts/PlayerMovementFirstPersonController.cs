﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementFirstPersonController : PlayerMovementController
{
    public PlayerMovementFirstPersonController(GameObject player, PlayerMovementData movementParameters) 
    : base(player, movementParameters)
    {  }

    public override void OnUpdate() {
        base.OnUpdate();
        player.transform.Rotate(Vector3.up * rotationInput.x);
    }

    protected override void Move()
    {
        if (movementInput != Vector2.zero)
        {
            var newVelocity = player.transform.forward * movementInput.y * movementParams.moveSpeed
                + player.transform.right * movementInput.x * movementParams.strafingSpeed;
            newVelocity.y = rigidbody.velocity.y;
            rigidbody.velocity = newVelocity;
        }
    }

    
}
